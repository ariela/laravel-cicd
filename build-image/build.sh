#!/bin/bash

PREVIOUS_DIR=$(pwd)
CURRENTS_DIR=$(cd $(dirname $0); pwd)
CMDNAME=$(basename $0)

export AWS_DEFAULT_REGION=ap-northeast-1
export AWS_PROFILE=kawamoto-cloudpack-jp

REPOSITORY_NAME=codebuild-php
REPOSITORY_ARN=744182368114.dkr.ecr.ap-northeast-1.amazonaws.com/${REPOSITORY_NAME}
IMAGE_TAG=7.1

cd ${CURRENTS_DIR}

$(aws ecr get-login --no-include-email)
if [ $? -ne 0 ]; then
    echo 'faild aws login'
    exit 1
fi

docker build -t ${REPOSITORY_ARN}:${IMAGE_TAG} ./
if [ $? -ne 0 ]; then
    echo 'faild build image'
    exit 1
fi

trivy --clear-cache
trivy --exit-code 1 ${REPOSITORY_ARN}:${IMAGE_TAG}
if [ $? -ne 0 ]; then
    echo 'finded vulnerability'
    exit 1
fi

docker push ${REPOSITORY_ARN}:${IMAGE_TAG}
if [ $? -ne 0 ]; then
    echo 'faild push image'
    exit 1
fi

../builder/cleaning.sh ${REPOSITORY_NAME}

cd ${PREVIOUS_DIR}

exit 0
