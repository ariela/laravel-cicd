#!/bin/bash
set -ue

#===+====1====+====2====+====3====+====4====+====5====+====6====+====7====+====8====+====9====+====0
# インストール処理
#===+====1====+====2====+====3====+====4====+====5====+====6====+====7====+====8====+====9====+====0

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# 変数定義
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# 呼び出しパス
PREVIOUS_DIR=$(pwd)
# スクリプト実行パス
CURRENTS_DIR=$(cd $(dirname $0)/../; pwd)


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# ログ出力処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
log () {
    echo "[$(date +"%Y-%m-%d %H:%M:%S %Z")][$1] $2"
}


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# DockerイメージPUSH処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
docker_image_push () {
    log "POST_BUILD" "Logging in to Amazon ECR..."
    $(aws ecr get-login --no-include-email)
    log "POST_BUILD" "Pushing the Docker image..."
    IMAGE_REPO_NAMES=($(echo $IMAGE_REPO_NAMES | tr ',' ' '))
    IMAGE_TAG_SUFFIXES=($(echo $IMAGE_TAG_SUFFIXES | tr ',' ' '))
    LEN=$((${#IMAGE_REPO_NAMES[@]} - 1))
    for idx in $(seq 0 $LEN); do
        IMAGE_REPO_NAME=${IMAGE_REPO_NAMES[$idx]}
        IMAGE_TAG=${IMAGE_TAG_SUFFIXES[$idx]}
        docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${IMAGE_REPO_NAME}:${IMAGE_TAG}
        ./builder/cleaning.sh ${IMAGE_REPO_NAME}
    done
}


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# メイン処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
cd ${CURRENTS_DIR}

log "POST_BUILD" "Post build started"
log "POST_BUILD" "Mode: ${BUILD_STAGE:=develop}"

if [ "${BUILD_STAGE}" = "develop" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # develop ステージ処理
    log "POST_BUILD" "Develop stage build."

elif [ "${BUILD_STAGE}" = "staging" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # staging ステージ処理
    log "POST_BUILD" "Staging stage build."
    docker_image_push

elif [ "${BUILD_STAGE}" = "production" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # production ステージ処理
    log "POST_BUILD" "Production stage build."
    docker_image_push

fi


log "POST_BUILD" "Post build finished"

cd ${PREVIOUS_DIR}

# 正常終了
exit 0
