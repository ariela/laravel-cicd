#!/bin/bash
set -ue

#===+====1====+====2====+====3====+====4====+====5====+====6====+====7====+====8====+====9====+====0
# インストール処理
#===+====1====+====2====+====3====+====4====+====5====+====6====+====7====+====8====+====9====+====0

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# 変数定義
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# 呼び出しパス
PREVIOUS_DIR=$(pwd)
# スクリプト実行パス
CURRENTS_DIR=$(cd $(dirname $0)/../; pwd)


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# ログ出力処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
log () {
    echo "[$(date +"%Y-%m-%d %H:%M:%S %Z")][$1] $2"
}


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# Dockerイメージビルド処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
docker_image_build () {
    log "BUILD" "Building the Docker image..."
    DOCKER_FILES=($(echo $DOCKER_FILES | tr ',' ' '))
    IMAGE_REPO_NAMES=($(echo $IMAGE_REPO_NAMES | tr ',' ' '))
    IMAGE_TAG_SUFFIXES=($(echo $IMAGE_TAG_SUFFIXES | tr ',' ' '))
    LEN=$((${#IMAGE_REPO_NAMES[@]} - 1))
    for idx in $(seq 0 $LEN); do
        IMAGE_REPO_NAME=${IMAGE_REPO_NAMES[$idx]}
        IMAGE_TAG=${IMAGE_TAG_SUFFIXES[$idx]}
        DOCKER_FILE=${DOCKER_FILES[$idx]}
        docker build -t ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${IMAGE_REPO_NAME}:${IMAGE_TAG} -f ${DOCKER_FILE} .
    done
}


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# メイン処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
cd ${CURRENTS_DIR}

log "BUILD" "Build started"
log "BUILD" "Mode: ${BUILD_STAGE:=develop}"

if [ "${BUILD_STAGE}" = "develop" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # develop ステージ処理
    log "BUILD" "Develop stage build."

elif [ "${BUILD_STAGE}" = "staging" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # staging ステージ処理
    log "BUILD" "Staging stage build."
    docker_image_build

elif [ "${BUILD_STAGE}" = "production" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # production ステージ処理
    log "BUILD" "Production stage build."
    docker_image_build
fi

log "BUILD" "Build finished"

cd ${PREVIOUS_DIR}

# 正常終了
exit 0
