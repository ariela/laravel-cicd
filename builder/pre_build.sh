#!/bin/bash
set -u

#===+====1====+====2====+====3====+====4====+====5====+====6====+====7====+====8====+====9====+====0
# インストール処理
#===+====1====+====2====+====3====+====4====+====5====+====6====+====7====+====8====+====9====+====0

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# 変数定義
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# 呼び出しパス
PREVIOUS_DIR=$(pwd)
# スクリプト実行パス
CURRENTS_DIR=$(cd $(dirname $0)/../; pwd)

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# ログ出力処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
log () {
    echo "[$(date +"%Y-%m-%d %H:%M:%S %Z")][$1] $2"
}

#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# PHP解析処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
php_inspections () {
    rm -rf /tmp/build
    mkdir -p /tmp/build/reports
    mkdir -p /tmp/build/tests

    cd ./sources

    composer install --optimize-autoloader
    php artisan ide-helper:generate

    phpcs \
        --standard=./phpcs.xml \
        --report=checkstyle \
        --report-file=/tmp/build/reports/phpcs.xml \
        ./

    phpcpd \
        --log-pmd=/tmp/build/reports/phpcpd.xml \
        --exclude=bootstrap/ \
        --exclude=config/ \
        --exclude=database/ \
        --exclude=public/ \
        --exclude=resources/ \
        --exclude=routes/ \
        --exclude=storage/ \
        --exclude=node_modules/ \
        --exclude=vendor/ \
        --exclude=server.php \
        --exclude=app/Console/Kernel.php \
        --exclude=tests/ \
        ./

    phpmd \
        ./ \
        xml \
        cleancode,codesize,controversial,design,naming,unusedcode \
        --reportfile /tmp/build/reports/phpmd.xml \
        --exclude bootstrap/,config/,database/,public/,resources/,routes/,storage/,node_modules/,vendor/,server.php,app/Console/Kernel.php,tests/

    phpstan \
        analyse \
        --no-interaction \
        --configuration phpstan.neon \
        ./

    phpunit \
        --coverage-clover /tmp/build/reports/phpunit.xml \
        --whitelist app/ \
        --log-junit /tmp/build/tests/phpunit.xml

    cd ..
}


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
# メイン処理
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
cd ${CURRENTS_DIR}

log "PRE_BUILD" "Pre build started"
log "PRE_BUILD" "Mode: ${BUILD_STAGE:=develop}"

if [ "${BUILD_STAGE}" = "develop" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # develop ステージ処理
    log "PRE_BUILD" "Develop stage build."
    php_inspections

elif [ "${BUILD_STAGE}" = "staging" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # staging ステージ処理
    log "PRE_BUILD" "Staging stage build."
    php_inspections

elif [ "${BUILD_STAGE}" = "production" ]; then
    #----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
    # production ステージ処理
    log "PRE_BUILD" "Production stage build."

fi


log "PRE_BUILD" "Pre build finished"

cd ${PREVIOUS_DIR}

# 正常終了
exit 0
