#!/bin/bash

log () {
    echo "[$(date +'%Y-%m-%d %H:%M:%S %Z')][INFO] $1"
}

error () {
    echo "[$(date +'%Y-%m-%d %H:%M:%S %Z')][ERROR] $1" 1>&2
}

if [ $# -lt 1 ]; then
    error "Please set repository name."
    exit 1
fi

REPOSITORY_NAME=$1
DELETE_COUNT=${2:-2}
log "Target Repository: ${REPOSITORY_NAME}"

log "Get images info."
OLD_IMAGES=($(aws ecr describe-images --repository-name ${REPOSITORY_NAME} --filter tagStatus=UNTAGGED --query 'sort_by(imageDetails[].{t:imagePushedAt, d:imageDigest},&t)[].d' --output text))

INDEX=$(expr ${#OLD_IMAGES[@]} - ${DELETE_COUNT})
if [ $INDEX -le 0 ]; then
    log "Old images not found."
    exit 0
fi

unset -v OLD_IMAGES[$INDEX]
OLD_IMAGES=( ${OLD_IMAGES[@]} )
DELETE_IMAGE=()
for i in ${!OLD_IMAGES[@]}; do
    DELETE_IMAGE=( ${DELETE_IMAGE[@]} "imageDigest=${OLD_IMAGES[$i]}" )
done

aws ecr batch-delete-image --repository-name ${REPOSITORY_NAME} --image-ids ${DELETE_IMAGE[@]}
